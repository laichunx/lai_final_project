﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb;
    SpriteRenderer sprite_renderer;
    GameObject frontObject;
    bool isGrounded;
    public GameObject spaceNotify;
    public GameObject gatherNotify;
    playerInventory playerInv;

    [SerializeField]
    Transform groundCheck;
    [SerializeField]
    Transform frontCheck;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite_renderer = GetComponent<SpriteRenderer>();
        playerInv = GetComponent<playerInventory>();
    }

    private void FixedUpdate()
    {
        //Debug.DrawLine(transform.position, groundCheck.position, Color.yellow);
        //Debug.DrawLine(transform.position + new Vector3(0,1,0), frontCheck.position, Color.green);
        if (Physics2D.Linecast(transform.position + new Vector3(0, 1, 0), frontCheck.position, 1 << LayerMask.NameToLayer("Interactive")))
        {
            frontObject = Physics2D.Linecast(transform.position + new Vector3(0, 1, 0), frontCheck.position, 1 << LayerMask.NameToLayer("Interactive")).collider.gameObject;
        }
        else
        {
            frontObject = null;
        }
            if (Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"))|| Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Interactive")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(3, rb.velocity.y);
            animator.Play("player_walk");
            sprite_renderer.flipX = false;
            frontCheck.position = new Vector3(rb.position.x + 1, rb.position.y,0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-3, rb.velocity.y);
            animator.Play("player_walk");
            sprite_renderer.flipX = true;
            frontCheck.position = new Vector3(rb.position.x - 1, rb.position.y, 0);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.Play("player_idle");
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, 8);
        }


        if (frontObject != null && frontObject.name == "grandma")
        {
            spaceNotify.GetComponent<Text>().enabled = true;
        }
        else
        {
            spaceNotify.GetComponent<Text>().enabled = false;
        }

        if (frontObject != null && Input.GetKeyDown(KeyCode.Space) && frontObject.name == "grandma")
        {
            //open store
        }

        if (frontObject != null && frontObject.tag == "resources")
        {
            gatherNotify.GetComponent<Text>().enabled = true;
        }
        else
        {
            gatherNotify.GetComponent<Text>().enabled = false;
        }

        if (frontObject != null && frontObject.tag == "resources" && Input.GetKeyDown(KeyCode.Space))
        {
            if (frontObject.name == "Iron")
            {
                playerInv.updateIron(10);
            }
            else if (frontObject.name == "gold")
            {
                playerInv.updateMoney(50);
            }
            else if (frontObject.name == "food")
            {
                playerInv.updateHealth(10);
                playerInv.updateEnergy(10);
            }
            else if (frontObject.name == "wood")
            {
                playerInv.updateWood(10);
            }
            Destroy(frontObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("ya");
    }
}