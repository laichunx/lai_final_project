﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crafting : MonoBehaviour
{
    public bool craftingEnabled;
    public GameObject crafting;
    public Text resultText;
    private int allSlots;
    private GameObject[] slot;
    Items itenInfo;
    public GameObject slotHolder;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            craftingEnabled = !craftingEnabled;
        }

        if(craftingEnabled == true)
        {
            crafting.SetActive(true);
        } else {
            crafting.SetActive(false);
        }
    }

    private void Start()
    {
        allSlots = 5;
        slot = new GameObject[allSlots];

        for (int i = 0; i < allSlots; i++)
        {
            slot[i] = slotHolder.transform.GetChild(i).gameObject;
            if (slot[i].GetComponent<Slot>().item == null)
            {
                slot[i].GetComponent<Slot>().empty = true;
            }
        }
    }


}
