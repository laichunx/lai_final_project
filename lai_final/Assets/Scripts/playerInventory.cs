﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerInventory : MonoBehaviour
{
    public bool inventoryEnabled;
    public GameObject inventory;
    
    private int allSlots;
    private int enabledSlots;
    private GameObject[] slot;
    public int money;
    public int iron;
    public int energy;
    public int wood;
    public int level;
    public int hp;
    public Text moneyText;
    public Text energyText;
    public Text ironText;
    public Text woodText;
    public Text levelText;
    public Text hpText;

    public GameObject slotHolder;

    void Start()
    {
        money = 500;
        iron = 200;
        energy = 100;
        wood = 200;
        level = 1;
        hp = 100;
        
        moneyText.text = "Money\n" + money.ToString();
        ironText.text = "Iron\n" + iron.ToString();
        energyText.text = "Energy\n" + energy.ToString();
        woodText.text = "Wood\n" + wood.ToString();
        levelText.text = "Level\n" + level.ToString();
        hpText.text = "Health\n" + hp.ToString();

        allSlots = 10;
        slot = new GameObject[allSlots];

        for(int i = 0; i < allSlots; i++)
        {
            slot[i] = slotHolder.transform.GetChild(i).gameObject;

            if(slot[i].GetComponent<Slot>().item == null)
            {
                slot[i].GetComponent<Slot>().empty = true;
            }
        }
        
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            inventoryEnabled = !inventoryEnabled;
        }

        if(inventoryEnabled == true)
        {
            inventory.SetActive(true);
        } else {
            inventory.SetActive(false);
        }
    }

    private void OnCollideEnter2D(Collider2D other)
    {
        if(other.tag == "items")
        {
            Debug.Log("Collide ");
            GameObject itemPickedUp = other.gameObject;
            Items item = itemPickedUp.GetComponent<Items>();
            
            AddItem(itemPickedUp, item.ID, item.type, item.desc, item.icon);
        }
    }

    public void AddItem(GameObject itemObject, int itemID, string itemType, string itemDesc, Sprite itemIcon)
    {
        for(int i = 0; i < allSlots; i++)
        {
            if(slot[i].GetComponent<Slot>().empty)
            {
                itemObject.GetComponent<Items>().pickedUp = true;

                slot[i].GetComponent<Slot>().icon = itemIcon;
                slot[i].GetComponent<Slot>().ID = itemID;
                slot[i].GetComponent<Slot>().type = itemType;
                slot[i].GetComponent<Slot>().desc = itemDesc;
                slot[i].GetComponent<Slot>().item = itemObject;

                itemObject.transform.parent = slot[i].transform;
                itemObject.SetActive(false);

                slot[i].GetComponent<Slot>().UpdateSlot();
                slot[i].GetComponent<Slot>().empty = false;
            }

            return;
        }
    }

    public void updateMoney(int amount)
    {
        money += amount;
        moneyText.text = "Money\n" + money.ToString();
    }

    public void updateIron(int amount)
    {
        iron += amount;
        ironText.text = "Iron\n" + iron.ToString();
    }

    public void updateLevel(int amount)
    {
        level += amount;
        levelText.text = "Level\n" + level.ToString();
    }

    public void updateHealth(int amount)
    {
        hp += amount;
        hpText.text = "Health\n" + hp.ToString();
    }

    public void updateWood(int amount)
    {
        wood += amount;
        woodText.text = "Wood\n" + wood.ToString();
    }

    public void updateEnergy(int amount)
    {
        energy += amount;
        energyText.text = "Energy\n" + energy.ToString();
    }
}
