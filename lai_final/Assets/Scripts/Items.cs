﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    public string type;
    public int ID;
    public string desc;
    public Sprite icon;
    public bool pickedUp;
}
