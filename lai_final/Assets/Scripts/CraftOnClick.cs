﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftOnClick : MonoBehaviour
{
    public Button SwordButton;
    public Button HelmetButton;
    public Button ArmorButton;
    public Button PantsButton;
    public Button BootsButton;
    Items itemInfo;
    playerInventory playerInv;

    void Start()
    {
        playerInv = GetComponent<playerInventory>();
        Button swordBtn = SwordButton.GetComponent<Button>();
        Button helmetBtn = HelmetButton.GetComponent<Button>();
        Button armorBtn = ArmorButton.GetComponent<Button>();
        Button pantsBtn = PantsButton.GetComponent<Button>();
        Button bootsBtn = BootsButton.GetComponent<Button>();
        swordBtn.onClick.AddListener(SwordGetItemInfo);
        helmetBtn.onClick.AddListener(HelmetGetItemInfo);
        armorBtn.onClick.AddListener(ArmorGetItemInfo);
        pantsBtn.onClick.AddListener(PantsGetItemInfo);
        bootsBtn.onClick.AddListener(BootsGetItemInfo);
    }

    void SwordGetItemInfo()
    {
        Debug.Log("Im in");
        if (playerInv.iron >= 10 && playerInv.wood >= 10 && playerInv.energy >= 10)
        {
            itemInfo = SwordButton.GetComponent<Items>();
            playerInv.AddItem(itemInfo.gameObject, itemInfo.ID, itemInfo.type, itemInfo.desc, itemInfo.icon);
            playerInv.updateIron(-10);
            playerInv.updateWood(-10);
            playerInv.updateEnergy(-10);
            Debug.Log("Im in2");
        }
    }

    void HelmetGetItemInfo()
    {
        if (playerInv.iron >= 10 && playerInv.wood >= 10 && playerInv.energy >= 10)
        {
            itemInfo = HelmetButton.GetComponent<Items>();
            playerInv.AddItem(itemInfo.gameObject, itemInfo.ID, itemInfo.type, itemInfo.desc, itemInfo.icon);
            playerInv.updateIron(-10);
            playerInv.updateWood(-10);
            playerInv.updateEnergy(-10);
        }
    }

    void ArmorGetItemInfo()
    {
        if (playerInv.iron >= 10 && playerInv.wood >= 10 && playerInv.energy >= 10)
        {
            itemInfo = ArmorButton.GetComponent<Items>();
            playerInv.AddItem(itemInfo.gameObject, itemInfo.ID, itemInfo.type, itemInfo.desc, itemInfo.icon);
            playerInv.updateIron(-10);
            playerInv.updateWood(-10);
            playerInv.updateEnergy(-10);
        }
    }

    void PantsGetItemInfo()
    {
        if (playerInv.iron >= 10 && playerInv.wood >= 10 && playerInv.energy >= 10)
        {
            itemInfo = PantsButton.GetComponent<Items>();
            playerInv.AddItem(itemInfo.gameObject, itemInfo.ID, itemInfo.type, itemInfo.desc, itemInfo.icon);
            playerInv.updateIron(-10);
            playerInv.updateWood(-10);
            playerInv.updateEnergy(-10);
        }
    }

    void BootsGetItemInfo()
    {
        if (playerInv.iron >= 10 && playerInv.wood >= 10 && playerInv.energy >= 10)
        {
            itemInfo = BootsButton.GetComponent<Items>();
            playerInv.AddItem(itemInfo.gameObject, itemInfo.ID, itemInfo.type, itemInfo.desc, itemInfo.icon);
            playerInv.updateIron(-10);
            playerInv.updateWood(-10);
            playerInv.updateEnergy(-10);
        }
    }
}
